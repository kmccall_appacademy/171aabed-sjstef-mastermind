class Code
    attr_reader :pegs, :code
    PEGS = {
          Red:    "R",
          Green:  "G",
          Blue:   "B",
          Yellow: "Y",
          Orange: "O",
          Purple: "P"
        }
  
    def initialize(pegs=PEGS.values)
      @pegs = pegs
    end
    
    def random
        @code = ""
        4.times do |time|
            code << @pegs.shuffle.first
        end
        @code
    end
    
    def parse
        puts "Please unlock the 4 color code consisting of RGBYOP"
        input = gets.chomp.upcase
        if input.length != 4
            "There must be 4 color choices, please re-enter your try"
            self.parse
        else
            return input
        end
    end
    
    def exact_matches(input)
        count = 0
        code = @code.chars
        input = input.chars
        
        input.each_with_index{|color, idx| count += 1 if code[idx] == color}
        return count
    end
    
    def near_matches(input)
        count = 0
        code = @code.chars
        input = input.chars
        
        input.each_with_index do |color, idx| 
            if code[idx] == color
                code.delete_at(idx)
                input.delete_at(idx)
            end
        end
        
        input.each{|color| count += 1 if code.include?(color)}
        return count
    end
        
end

class Game
  attr_reader :secret_code
  
  def initialize(secret_code=nil)
      @coder = Code.new
      @secret_code = secret_code || @coder.random
  end
  
  def play
      guesses = 10
      until guesses == 0
          puts "You have #{guesses} guesses"
          self.get_guess
          exact = @coder.exact_matches(@input)
          self.display_matches(@input)
          return "Congratulations you've cracked the code!" if exact == 4
          guesses = guesses - 1
      end
      return "Sorry, you are out of guesses and have not cracked the code" if guesses == 0
  end
  
  def get_guess
      @input = @coder.parse
      return @input
  end
  
  def display_matches(input)
      exact = @coder.exact_matches(@input)
      near = @coder.near_matches(@input)
      puts "You have guessed #{exact} colors in their correct position, and #{near} colors that are in the secret code but in an incorrect position."
  end

end
